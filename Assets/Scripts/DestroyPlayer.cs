﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class DestroyPlayer : MonoBehaviour {

    public Button restartButton;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Player")) {
            Destroy (other.gameObject);
            restartButton.gameObject.SetActive(true);
            //SceneManager.LoadScene("MainScene");
        }
	}
}
