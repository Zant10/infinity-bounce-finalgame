﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    public float timeToLoadScene = 10;
    
    void Start()
    {
        Invoke("GoToScene", timeToLoadScene);
    }
    void GoToScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Restart()
    {
        SceneManager.LoadScene("MainScene");
    }
}
