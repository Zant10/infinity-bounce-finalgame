﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    public float ballSpeed = 40;
    public Vector3 ballInitialVelocity;
    public Button playButton;
    public Text countText;
    public GameObject countParticles;
    public GameObject superParticles;
    public GameObject plusParticles;


    private Rigidbody rb;
    //private Collider co;
    private int count;
    private Vector2 BallDirection
    { get { return rb.velocity.normalized; } }


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        //co = GetComponent<Collider>();
        SetCountText();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Color Space"))
        {
            Instantiate(countParticles, transform.position, Quaternion.identity);
            Instantiate(plusParticles, transform.position, Quaternion.identity);
            count = count + 1;
            SetCountText();
        }
        if (other.gameObject.CompareTag("Super Color"))
        {
            Instantiate(superParticles, transform.position, Quaternion.identity);
            count = count + 1;
            SetCountText();
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Power Up"))
        {
            StartCoroutine(PowerUpRoutine());
            Destroy(other.gameObject);
        }

    }

    public void Play()
    {
        rb.isKinematic = false;
        rb.velocity = ballInitialVelocity.normalized * ballSpeed;
    }

    public void HideButton()
    {
        playButton.gameObject.SetActive(false);
    }

    private void SetCountText()
    {
        countText.text = " " + count.ToString();
    }

    private IEnumerator PowerUpRoutine()
    {
        rb.velocity = BallDirection * ballSpeed*2f;
        //co.enabled = !co.enabled;
        yield return new WaitForSeconds(2f);
        rb.velocity = BallDirection * ballSpeed;    
    }


}