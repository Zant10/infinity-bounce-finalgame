﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTouchController : MonoBehaviour
{
    [Range(0.1f,1)]
    [SerializeField]
    private float reactive = 0.5f;

    public float speed;

    private float xBasePos;

    private float xRelativePos;

    

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            UpdateBasePos();
        }
        if (Input.GetMouseButton(0))
        {
            UpdateInputPos();
            UpdatePosition();
        }
#elif UNITY_ANDROID
        Touch touch = Input.GetTouch(0);

        if (touch.phase == TouchPhase.Began )
        {
            UpdateBasePos();
        }
        else if (touch.phase == TouchPhase.Moved)
        {
            UpdateInputPos();
            UpdatePosition();
        }
#endif

    }

    private void UpdateInputPos()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = -Camera.main.transform.position.z;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        xRelativePos = mousePos.x - xBasePos;
    }

    private void UpdateBasePos()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = -Camera.main.transform.position.z;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        xBasePos = mousePos.x;
        print("xBasePos: " + xBasePos);
    }

    private void UpdatePosition()
    {

        float direction = Mathf.Sign(xRelativePos);
        float magnitud = xRelativePos * direction;

        Vector3 displacement = Vector3.right * xRelativePos * reactive;
        transform.position += displacement * speed;
        xBasePos += displacement.x;
    }

}
